// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Fighting_Game_testGameMode.generated.h"

UCLASS(minimalapi)
class AFighting_Game_testGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFighting_Game_testGameMode();
};



