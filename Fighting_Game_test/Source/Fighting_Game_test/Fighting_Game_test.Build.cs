// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Fighting_Game_test : ModuleRules
{
	public Fighting_Game_test(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });
	}
}
