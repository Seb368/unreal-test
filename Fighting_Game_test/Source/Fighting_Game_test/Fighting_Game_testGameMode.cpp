// Copyright Epic Games, Inc. All Rights Reserved.

#include "Fighting_Game_testGameMode.h"
#include "Fighting_Game_testCharacter.h"
#include "UObject/ConstructorHelpers.h"

AFighting_Game_testGameMode::AFighting_Game_testGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/SideScrollerCPP/Blueprints/SideScrollerCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
