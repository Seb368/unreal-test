// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIGHTING_GAME_TEST_Hitbox_Actor_generated_h
#error "Hitbox_Actor.generated.h already included, missing '#pragma once' in Hitbox_Actor.h"
#endif
#define FIGHTING_GAME_TEST_Hitbox_Actor_generated_h

#define Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_SPARSE_DATA
#define Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_RPC_WRAPPERS
#define Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAHitbox_Actor(); \
	friend struct Z_Construct_UClass_AHitbox_Actor_Statics; \
public: \
	DECLARE_CLASS(AHitbox_Actor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Fighting_Game_test"), NO_API) \
	DECLARE_SERIALIZER(AHitbox_Actor)


#define Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_INCLASS \
private: \
	static void StaticRegisterNativesAHitbox_Actor(); \
	friend struct Z_Construct_UClass_AHitbox_Actor_Statics; \
public: \
	DECLARE_CLASS(AHitbox_Actor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Fighting_Game_test"), NO_API) \
	DECLARE_SERIALIZER(AHitbox_Actor)


#define Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AHitbox_Actor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AHitbox_Actor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHitbox_Actor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHitbox_Actor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHitbox_Actor(AHitbox_Actor&&); \
	NO_API AHitbox_Actor(const AHitbox_Actor&); \
public:


#define Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHitbox_Actor(AHitbox_Actor&&); \
	NO_API AHitbox_Actor(const AHitbox_Actor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHitbox_Actor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHitbox_Actor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AHitbox_Actor)


#define Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_PRIVATE_PROPERTY_OFFSET
#define Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_16_PROLOG
#define Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_PRIVATE_PROPERTY_OFFSET \
	Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_SPARSE_DATA \
	Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_RPC_WRAPPERS \
	Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_INCLASS \
	Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_PRIVATE_PROPERTY_OFFSET \
	Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_SPARSE_DATA \
	Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_INCLASS_NO_PURE_DECLS \
	Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIGHTING_GAME_TEST_API UClass* StaticClass<class AHitbox_Actor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Fighting_Game_test_Source_Fighting_Game_test_Hitbox_Actor_h


#define FOREACH_ENUM_EHITBOXENUM(op) \
	op(EHitboxEnum::HB_PROXIMITY) \
	op(EHitboxEnum::HB_STRIKE) \
	op(EHitboxEnum::HB_HURTBOX) 

enum class EHitboxEnum : uint8;
template<> FIGHTING_GAME_TEST_API UEnum* StaticEnum<EHitboxEnum>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
