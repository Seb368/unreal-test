// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIGHTING_GAME_TEST_Fighting_Game_testGameMode_generated_h
#error "Fighting_Game_testGameMode.generated.h already included, missing '#pragma once' in Fighting_Game_testGameMode.h"
#endif
#define FIGHTING_GAME_TEST_Fighting_Game_testGameMode_generated_h

#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_SPARSE_DATA
#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_RPC_WRAPPERS
#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFighting_Game_testGameMode(); \
	friend struct Z_Construct_UClass_AFighting_Game_testGameMode_Statics; \
public: \
	DECLARE_CLASS(AFighting_Game_testGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Fighting_Game_test"), FIGHTING_GAME_TEST_API) \
	DECLARE_SERIALIZER(AFighting_Game_testGameMode)


#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAFighting_Game_testGameMode(); \
	friend struct Z_Construct_UClass_AFighting_Game_testGameMode_Statics; \
public: \
	DECLARE_CLASS(AFighting_Game_testGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Fighting_Game_test"), FIGHTING_GAME_TEST_API) \
	DECLARE_SERIALIZER(AFighting_Game_testGameMode)


#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	FIGHTING_GAME_TEST_API AFighting_Game_testGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFighting_Game_testGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FIGHTING_GAME_TEST_API, AFighting_Game_testGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFighting_Game_testGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FIGHTING_GAME_TEST_API AFighting_Game_testGameMode(AFighting_Game_testGameMode&&); \
	FIGHTING_GAME_TEST_API AFighting_Game_testGameMode(const AFighting_Game_testGameMode&); \
public:


#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FIGHTING_GAME_TEST_API AFighting_Game_testGameMode(AFighting_Game_testGameMode&&); \
	FIGHTING_GAME_TEST_API AFighting_Game_testGameMode(const AFighting_Game_testGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FIGHTING_GAME_TEST_API, AFighting_Game_testGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFighting_Game_testGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFighting_Game_testGameMode)


#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_9_PROLOG
#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_SPARSE_DATA \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_RPC_WRAPPERS \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_INCLASS \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_SPARSE_DATA \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_INCLASS_NO_PURE_DECLS \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIGHTING_GAME_TEST_API UClass* StaticClass<class AFighting_Game_testGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
