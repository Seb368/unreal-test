// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Fighting_Game_test/Hitbox_Actor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHitbox_Actor() {}
// Cross Module References
	FIGHTING_GAME_TEST_API UEnum* Z_Construct_UEnum_Fighting_Game_test_EHitboxEnum();
	UPackage* Z_Construct_UPackage__Script_Fighting_Game_test();
	FIGHTING_GAME_TEST_API UClass* Z_Construct_UClass_AHitbox_Actor_NoRegister();
	FIGHTING_GAME_TEST_API UClass* Z_Construct_UClass_AHitbox_Actor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	static UEnum* EHitboxEnum_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Fighting_Game_test_EHitboxEnum, Z_Construct_UPackage__Script_Fighting_Game_test(), TEXT("EHitboxEnum"));
		}
		return Singleton;
	}
	template<> FIGHTING_GAME_TEST_API UEnum* StaticEnum<EHitboxEnum>()
	{
		return EHitboxEnum_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EHitboxEnum(EHitboxEnum_StaticEnum, TEXT("/Script/Fighting_Game_test"), TEXT("EHitboxEnum"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Fighting_Game_test_EHitboxEnum_Hash() { return 4054552670U; }
	UEnum* Z_Construct_UEnum_Fighting_Game_test_EHitboxEnum()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Fighting_Game_test();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EHitboxEnum"), 0, Get_Z_Construct_UEnum_Fighting_Game_test_EHitboxEnum_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EHitboxEnum::HB_PROXIMITY", (int64)EHitboxEnum::HB_PROXIMITY },
				{ "EHitboxEnum::HB_STRIKE", (int64)EHitboxEnum::HB_STRIKE },
				{ "EHitboxEnum::HB_HURTBOX", (int64)EHitboxEnum::HB_HURTBOX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "HB_HURTBOX.DisplayName", "Hurtbox" },
				{ "HB_HURTBOX.Name", "EHitboxEnum::HB_HURTBOX" },
				{ "HB_PROXIMITY.DisplayName", "Proximity" },
				{ "HB_PROXIMITY.Name", "EHitboxEnum::HB_PROXIMITY" },
				{ "HB_STRIKE.DisplayName", "Strike" },
				{ "HB_STRIKE.Name", "EHitboxEnum::HB_STRIKE" },
				{ "ModuleRelativePath", "Hitbox_Actor.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Fighting_Game_test,
				nullptr,
				"EHitboxEnum",
				"EHitboxEnum",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void AHitbox_Actor::StaticRegisterNativesAHitbox_Actor()
	{
	}
	UClass* Z_Construct_UClass_AHitbox_Actor_NoRegister()
	{
		return AHitbox_Actor::StaticClass();
	}
	struct Z_Construct_UClass_AHitbox_Actor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_hitboxType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_hitboxType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_hitboxType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_hitboxLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_hitboxLocation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AHitbox_Actor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Fighting_Game_test,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHitbox_Actor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Hitbox_Actor.h" },
		{ "ModuleRelativePath", "Hitbox_Actor.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AHitbox_Actor_Statics::NewProp_hitboxType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHitbox_Actor_Statics::NewProp_hitboxType_MetaData[] = {
		{ "Category", "Hitbox" },
		{ "Comment", "//The hitbox instance\n" },
		{ "ModuleRelativePath", "Hitbox_Actor.h" },
		{ "ToolTip", "The hitbox instance" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AHitbox_Actor_Statics::NewProp_hitboxType = { "hitboxType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AHitbox_Actor, hitboxType), Z_Construct_UEnum_Fighting_Game_test_EHitboxEnum, METADATA_PARAMS(Z_Construct_UClass_AHitbox_Actor_Statics::NewProp_hitboxType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AHitbox_Actor_Statics::NewProp_hitboxType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHitbox_Actor_Statics::NewProp_hitboxLocation_MetaData[] = {
		{ "Category", "Hitbox" },
		{ "Comment", "//The hitbox location\n" },
		{ "ModuleRelativePath", "Hitbox_Actor.h" },
		{ "ToolTip", "The hitbox location" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AHitbox_Actor_Statics::NewProp_hitboxLocation = { "hitboxLocation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AHitbox_Actor, hitboxLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_AHitbox_Actor_Statics::NewProp_hitboxLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AHitbox_Actor_Statics::NewProp_hitboxLocation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AHitbox_Actor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHitbox_Actor_Statics::NewProp_hitboxType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHitbox_Actor_Statics::NewProp_hitboxType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHitbox_Actor_Statics::NewProp_hitboxLocation,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AHitbox_Actor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AHitbox_Actor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AHitbox_Actor_Statics::ClassParams = {
		&AHitbox_Actor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AHitbox_Actor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AHitbox_Actor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AHitbox_Actor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AHitbox_Actor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AHitbox_Actor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AHitbox_Actor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AHitbox_Actor, 882789081);
	template<> FIGHTING_GAME_TEST_API UClass* StaticClass<AHitbox_Actor>()
	{
		return AHitbox_Actor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AHitbox_Actor(Z_Construct_UClass_AHitbox_Actor, &AHitbox_Actor::StaticClass, TEXT("/Script/Fighting_Game_test"), TEXT("AHitbox_Actor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AHitbox_Actor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
