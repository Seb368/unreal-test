// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIGHTING_GAME_TEST_Fighting_Game_testCharacter_generated_h
#error "Fighting_Game_testCharacter.generated.h already included, missing '#pragma once' in Fighting_Game_testCharacter.h"
#endif
#define FIGHTING_GAME_TEST_Fighting_Game_testCharacter_generated_h

#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_SPARSE_DATA
#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_RPC_WRAPPERS
#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFighting_Game_testCharacter(); \
	friend struct Z_Construct_UClass_AFighting_Game_testCharacter_Statics; \
public: \
	DECLARE_CLASS(AFighting_Game_testCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Fighting_Game_test"), NO_API) \
	DECLARE_SERIALIZER(AFighting_Game_testCharacter)


#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAFighting_Game_testCharacter(); \
	friend struct Z_Construct_UClass_AFighting_Game_testCharacter_Statics; \
public: \
	DECLARE_CLASS(AFighting_Game_testCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Fighting_Game_test"), NO_API) \
	DECLARE_SERIALIZER(AFighting_Game_testCharacter)


#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFighting_Game_testCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFighting_Game_testCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFighting_Game_testCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFighting_Game_testCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFighting_Game_testCharacter(AFighting_Game_testCharacter&&); \
	NO_API AFighting_Game_testCharacter(const AFighting_Game_testCharacter&); \
public:


#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFighting_Game_testCharacter(AFighting_Game_testCharacter&&); \
	NO_API AFighting_Game_testCharacter(const AFighting_Game_testCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFighting_Game_testCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFighting_Game_testCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFighting_Game_testCharacter)


#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SideViewCameraComponent() { return STRUCT_OFFSET(AFighting_Game_testCharacter, SideViewCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AFighting_Game_testCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__wasFirstAttackUsed() { return STRUCT_OFFSET(AFighting_Game_testCharacter, wasFirstAttackUsed); } \
	FORCEINLINE static uint32 __PPO__playerHealth() { return STRUCT_OFFSET(AFighting_Game_testCharacter, playerHealth); }


#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_9_PROLOG
#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_SPARSE_DATA \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_RPC_WRAPPERS \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_INCLASS \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_SPARSE_DATA \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_INCLASS_NO_PURE_DECLS \
	Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIGHTING_GAME_TEST_API UClass* StaticClass<class AFighting_Game_testCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Fighting_Game_test_Source_Fighting_Game_test_Fighting_Game_testCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
